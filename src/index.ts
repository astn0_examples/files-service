import fs from 'fs'
import amqp from 'amqplib/callback_api'
/**
 * Скрипт для удаления файлов
 * @param fileName
 */
const deleteFile = async (fileName: string): Promise<void> => {
  console.log('Worker: удаляю файл ', fileName)
  await new Promise((resolve, reject) => {
    fs.unlink('./uploads/' + fileName, (err) => {
      if (err) {
        reject(err)
      } else {
        resolve('ok')
      }
    })
  }).catch((err) => {
    console.log('нет такого файла')
  })
}

amqp.connect('amqp://rabbitmq', (error0, connection) => {
  if (error0) {
    throw error0
  }
  connection.createChannel((error1, channel) => {
    if (error1) {
      throw error1
    }
    const queue: string = 'delete_file' // todo: в конфиг

    channel.assertQueue(queue, {
      durable: false,
    })

    channel.consume(
      queue,
      (msg: any) => {
        console.log('Worker: Received ', msg.content.toString())
        deleteFile(msg.content.toString())
      },
      {
        noAck: true,
      }
    )
  })
})
